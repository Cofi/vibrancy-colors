#!/bin/bash
#
# Fetches and extracts specified XFCE 4.12 source package, allowing
# us to keep only /debian for it in git.
#
# 2015 Filip Danilovic <filip@openmailbox.org>


# Set the package version and name:
NAME=vibrancy-colors
VER=2.6
PKG=$NAME\_$VER~trusty~Noobslab.com.tar.gz


# Set the directory in which to download/extract
# This is meant for workspace/build dir on Jenkins!
DIR=jenkins-build


# If directory is set and exists, change to it.

if [ -z $DIR ] && [ -d $DIR ];
then
	cd $DIR
fi


# If no argument is given, default to just downloading the package.

if [ ! -f $PKG ]; 
then
	wget https://launchpad.net/~ravefinity-project/+archive/ubuntu/ppa/+files/$PKG
else
	echo "$PKG exists, skipping download"
fi


# If argument is "-x", download and extract package.

if [ "$1" = "-x" ];
then
	if [ ! -f $PKG ]; 
	then
		wget https://launchpad.net/~ravefinity-project/+archive/ubuntu/ppa/+files/$PKG
	else
		echo "$PKG exists, skipping download"
	fi
	tar xvzf $PKG --exclude='debian' --strip 1
fi


# If arguments are "-x" & "-r", download, extract and remove package.
# Might be needed to prevent dpkg from complaining during package building.

if [ "$1" = "-x" ] && [ "$2" = "-r" ];
then
	if [ ! -f $PKG ]; 
	then
		wget https://launchpad.net/~ravefinity-project/+archive/ubuntu/ppa/+files/$PKG
	else
		echo "$PKG found, skipping download"
	fi
	tar xvzf $PKG --exclude='debian' --strip 1 && rm -v $PKG
fi


# If arguments are "-x" & "-m", download, extract and move package. Also remove self & Readme.md.
# This is what we use for Jenkins build.

if [ "$1" = "-x" ] && [ "$2" = "-m" ];
then
	if [ ! -f $PKG ]; 
	then
		wget https://launchpad.net/~ravefinity-project/+archive/ubuntu/ppa/+files/$PKG
	else
		echo "$PKG found, skipping download"
	fi

	mkdir ./new-version

	tar xvzf $PKG --exclude='debian' --strip 1 -C ./new-version && mv -f -v $PKG ../$NAME\_$VER.orig.tar.gz

	mv -f -v ./Vibrancy-Colors/*-TRIOS ./new-version/Vibrancy-Colors/

	rm -f -r ./Vibrancy-Colors && mv ./new-version/Vibrancy-Colors/ ./
	rm -f -r ./new-version && rm -f ./README.md

	for dir in $(find ./Vibrancy-Colors -type f -name "distributor-logo.svg" | rev | cut -d'/' -f 2- | rev); do
		cp -f -v ./trios-dist-logo/distributor-logo.svg "$dir"/distributor-logo.svg
		cp -f -v ./trios-dist-logo/distributor-logo.svg "$dir"/start-here.svg
	done

	rm -f -- "$0"

fi
